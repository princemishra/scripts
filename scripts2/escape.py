import string
text = 'prince.mishra@druva.com'
chars = ['.','@']
replaceWith = 'span'
for char in chars:
    text = string.replace(text, char, '<'+replaceWith+'>'+char+'</'+replaceWith+'>')
print text